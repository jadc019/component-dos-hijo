{
  const {
    html,
  } = Polymer;
  /**
    `<component-dos-hijo>` Description.

    Example:

    ```html
    <component-dos-hijo></component-dos-hijo>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --component-dos-hijo | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class ComponentDosHijo extends Polymer.Element {

    static get is() {
      return 'component-dos-hijo';
    }

    static get properties() {
      return {
        text2: {
          type: String,
          notify: true
        }
      };
    }

    static get template() {
      return html `
      <style include="component-dos-hijo-styles component-dos-hijo-shared-styles"></style>
      <slot></slot>
      
          <cells-molecule-input label="Text2" value="{{text2}}"></cells-molecule-input>
      
      `;
    }
  }

  customElements.define(ComponentDosHijo.is, ComponentDosHijo);
}